import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/habitation.dart';
import '../share/location_style.dart';
import '../share/location_text_style.dart';

class ResaLocation extends StatefulWidget {
  final Habitation _habitation;
  const ResaLocation(this._habitation, {Key? key}) : super(key: key);

  @override
  _ResaLocationState createState() => _ResaLocationState();
}

class OptionPayanteCheck extends OptionPayante {
  bool checked;
  OptionPayanteCheck(super.id, super.libelle, this.checked,
      {super.description = "", super.prix});
}

class DropdownButtonExample extends StatefulWidget {
  const DropdownButtonExample({super.key});

  @override
  State<DropdownButtonExample> createState() => _DropdownButtonExampleState();
}

class _DropdownButtonExampleState extends State<DropdownButtonExample> {
  final List<String> list = ['1', '2', '3', '4', '5', '6', '7', '8'];
  String dropdownValue = '1';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? value) {
        // This is called when the user selects an item.
        setState(() {
          dropdownValue = value!;
        });
      },
      items: list.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}

class _ResaLocationState extends State<ResaLocation> {
  DateTime dateDebut = DateTime.now();
  DateTime dateFin = DateTime.now();
  String nbPersonnes = '1';
  List<OptionPayanteCheck> optionPayanteChecks = [];
  var format = NumberFormat("### €");
  var prixTotal = 0;

  @override
  Widget build(BuildContext context) {
    if (optionPayanteChecks.isEmpty) {
      optionPayanteChecks = _loadOptionPayantes(widget._habitation);
    }
    prixTotal = _getTotalPrice(widget._habitation, optionPayanteChecks);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Réservation'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(4.0),
        children: [
          _buildResume(widget._habitation),
          _buildDates(dateDebut, dateFin),
          _buildNbPersonnes(),
          _buildOptionsPayantes(context),
          TotalWidget(prixTotal),
          _buildRentButton(),
        ],
      ),
    );
  }

  dateTimeRangePicker() async {
    DateTimeRange? datePicked = await showDateRangePicker(
      context: context,
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 2),
      initialDateRange: DateTimeRange(start: dateDebut, end: dateFin),
      cancelText: 'Annuler',
      confirmText: 'Valider',
    );
    if (datePicked != null) {
      setState(() {
        dateDebut = datePicked.start;
        dateFin = datePicked.end;
      });
    }
  }

  List<OptionPayanteCheck> _loadOptionPayantes(Habitation habitation) {
    return habitation.optionspayantes
        .map((option) => OptionPayanteCheck(option.id, option.libelle, false,
            description: option.description, prix: option.prix))
        .toList();
  }

  _getTotalPrice(
      Habitation habitation, List<OptionPayanteCheck> optionPayanteChecks) {
    double totalPrice = 0;
    for (var option in optionPayanteChecks) {
      if (option.checked == true) {
        totalPrice = totalPrice + option.prix;
      }
    }
    totalPrice += habitation.prixmois;
    return totalPrice;
  }

  _buildResume(Habitation habitation) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      color: Colors.grey[300],
      child: Row(
        children: [
          const Expanded(flex: 1, child: Icon(Icons.house)),
          Expanded(
            flex: 3,
            child: ListTile(
              title: Text(habitation.libelle),
              subtitle: Text(habitation.adresse),
            ),
          ),
        ],
      ),
    );
  }

  _buildDates(dateDebut, dateFin) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () {
              dateTimeRangePicker();
            },
            child: Row(
              children: [
                const Icon(Icons.date_range),
                Text(DateFormat('yyyy-MM-dd – kk:mm').format(dateDebut)),
              ],
            ),
          ),
          CircleAvatar(
            backgroundColor: Colors.brown.shade800,
            child: const Icon(Icons.arrow_forward),
          ),
          Row(
            children: [
              const Icon(Icons.date_range),
              Text(DateFormat('yyyy-MM-dd – kk:mm').format(dateFin)),
            ],
          ),
        ],
      ),
    );
  }

  _buildNbPersonnes() {
    return Container(
      padding: const EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [Text('Nombre de personnes '), DropdownButtonExample()],
      ),
    );
  }

  _buildOptionsPayantes(context) {
    return Column(
      children: Iterable.generate(optionPayanteChecks.length, (index) {
        return CheckboxListTile(
          title: Text(optionPayanteChecks[index].libelle +
              "(" +
              optionPayanteChecks[index].prix.toString() +
              " €)"),
          subtitle: Text(optionPayanteChecks[index].description),
          value: optionPayanteChecks[index].checked,
          onChanged: (newValue) {
            setState(() {
              optionPayanteChecks[index].checked = newValue!;
            });
          },
        );
      }).toList(),
    );
  }

  TotalWidget(prixTotal) {
    return Container(
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.purple),
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: Container(
                  alignment: Alignment.centerRight,
                  child: Text('TOTAL ',
                      style: LocationTextStyle.priceGreyTextStyle))),
          Expanded(
              flex: 2,
              child: Text(prixTotal.toString() + '€',
                  style: LocationTextStyle.priceGreyTextStyle))
        ],
      ),
    );
  }

  _buildRentButton() {
    return Container(
      padding: EdgeInsets.all(6.0),
      height: 100,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Expanded(
          child: Container(
            height: 80,
            decoration: BoxDecoration(
              color: LocationStyle.backgroundColorPurple,
              borderRadius: BorderRadius.circular(8.0),
            ),
            margin: EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                print('hello');
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 5),
                  Text(
                    'Louer',
                    style: LocationTextStyle.regularWhiteTextStyle,
                  )
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
