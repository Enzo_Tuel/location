import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:location/views/share/bottom_navigation_bar_widget.dart';

import '../models/location.dart';
import '../services/location_service.dart';

class LocationList extends StatefulWidget {
  static String routeName = 'locations';
  const LocationList({Key? key}) : super(key: key);

  @override
  State<LocationList> createState() => _LocationListState();
}

class _LocationListState extends State<LocationList> {
  final LocationService service = LocationService();
  late Future<List<Location>> _locations;

  @override
  void initState() {
    super.initState();
    _locations = service.getLocations();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mes locations'),
      ),
      body: Center(
        child: FutureBuilder<List<Location>>(
            future: _locations, // a previously-obtained Future<String> or null
            builder: (context, snapshot) {
              List<Widget> children;
              if (snapshot.hasData) {
                children = <Widget>[
                  const Icon(
                    Icons.check_circle_outline,
                    color: Colors.green,
                    size: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                  ),
                ];
              } else if (snapshot.hasError) {
                children = <Widget>[
                  const Icon(
                    Icons.error_outline,
                    color: Colors.red,
                    size: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text('Error: ${snapshot.error}'),
                  ),
                ];
              } else {
                children = const <Widget>[
                  SizedBox(
                    width: 60,
                    height: 60,
                    child: CircularProgressIndicator(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text('Awaiting result...'),
                  ),
                ];
              }
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: children,
                ),
              );
            }),
      ),
      bottomNavigationBar: const BottomNavigationBarWidget(2),
    );
  }

  _buildRow(Location location, BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4.0),
      child: Column(
        children: [
          _buildResume(location),
        ],
      ),
    );
  }

  _buildResume(Location location) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      color: Colors.grey[300],
      child: Row(
        children: [
          const Expanded(flex: 1, child: Icon(Icons.house)),
          Expanded(
            flex: 3,
            child: ListTile(
              title: Text(location.libelle),
              subtitle: Text(location.adresse),
            ),
          ),
        ],
      ),
    );
  }

  _buildDates(dateDebut, dateFin) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            child: Row(
              children: [
                const Icon(Icons.date_range),
                Text(DateFormat('yyyy-MM-dd – kk:mm').format(dateDebut)),
              ],
            ),
          ),
          CircleAvatar(
            backgroundColor: Colors.brown.shade800,
            child: const Icon(Icons.arrow_forward),
          ),
          Row(
            children: [
              const Icon(Icons.date_range),
              Text(DateFormat('yyyy-MM-dd – kk:mm').format(dateFin)),
            ],
          ),
        ],
      ),
    );
  }
}
