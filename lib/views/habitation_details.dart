import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:location/views/resa_location.dart';
import 'package:location/views/share/habitation_features_widget.dart';

import '../models/habitation.dart';
import '../share/location_style.dart';
import '../share/location_text_style.dart';

class HabitationDetails extends StatefulWidget {
  final Habitation _habitation;

  const HabitationDetails(this._habitation, {Key? key}) : super(key: key);

  @override
  _HabitationDetailsState createState() => _HabitationDetailsState();
}

class _HabitationDetailsState extends State<HabitationDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._habitation.libelle),
      ),
      body: ListView(
        padding: EdgeInsets.all(4.0),
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              'assets/images/locations/${widget._habitation.image}',
              fit: BoxFit.fitWidth,
            ),
          ),
          Container(
            margin: EdgeInsets.all(8.0),
            child: Text(widget._habitation.adresse),
          ),
          HabitationFeaturesWidget(widget._habitation),
          _buildItems(),
          _buildOptionsPayantes(),
          _buildRentButton(),
        ],
      ),
    );
  }

  _buildRentButton() {
    var format = NumberFormat("### €");

    return Container(
      decoration: BoxDecoration(
        color: LocationStyle.backgroundColorPurple,
        borderRadius: BorderRadius.circular(8.0),
      ),
      margin: EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              format.format(widget._habitation.prixmois),
              style: LocationTextStyle.priceTextStyle,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 8.0),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ResaLocation(widget._habitation)),
                );
              },
              child: Text('Réserver'),
            ),
          ),
        ],
      ),
    );
  }

  _buildItems() {
    var width = (MediaQuery.of(context).size.width / 2) - 15;

    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            children: [
              Container(
                width: 80,
                padding: EdgeInsets.only(left: 8),
                child: Text(
                  "Inclus",
                  style: LocationTextStyle.baseTextStyle,
                ),
              ),
              Expanded(
                child: Divider(
                  height: 36,
                  color: Colors.grey[300],
                ),
              ),
            ],
          ),
        ),
        Wrap(
          spacing: 2.0,
          children: Iterable.generate(
            widget._habitation.options.length,
            (i) => Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 12),
                    margin: EdgeInsets.symmetric(horizontal: 2.0),
                    child: Text(
                      widget._habitation.options[i].libelle,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 12),
                    margin: EdgeInsets.symmetric(horizontal: 2.0),
                    child: Text(
                      widget._habitation.options[i].description,
                      style: LocationTextStyle.regularGreyTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ).toList(),
        ),
      ],
    );
  }

  _buildOptionsPayantes() {
    var width = (MediaQuery.of(context).size.width / 2) - 15;
    var format = NumberFormat("### €");
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            children: [
              Container(
                width: 80,
                padding: EdgeInsets.only(left: 8),
                child: Text(
                  "Option",
                  style: LocationTextStyle.baseTextStyle,
                ),
              ),
              Expanded(
                child: Divider(
                  height: 36,
                  color: Colors.grey[300],
                ),
              ),
            ],
          ),
        ),
        Wrap(
          spacing: 2.0,
          children: Iterable.generate(
            widget._habitation.options.length,
            (i) => Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 2.0),
                    padding: EdgeInsets.only(left: 12),
                    child: Text(
                      widget._habitation.optionspayantes[i].libelle,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 2.0),
                    padding: EdgeInsets.only(left: 12),
                    child: Text(
                      format.format(widget._habitation.optionspayantes[i].prix),
                      style: LocationTextStyle.regularGreyTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ).toList(),
        ),
      ],
    );
  }

  _buildDividers() {
    return Row(
      children: [
        Container(
          child: Text('TEST'),
        )
      ],
    );
  }
}
