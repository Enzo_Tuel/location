import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:location/views/share/bottom_navigation_bar_widget.dart';

class Profil extends StatefulWidget {
  static String routeName = 'profil';
  const Profil({Key? key}) : super(key: key);

  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text('Profil à faire'),
      bottomNavigationBar: const BottomNavigationBarWidget(3),
    );
  }
}
