import '../models/location.dart';
import '../models/location_api_client.dart';
import '../models/location_api_data.dart';

class LocationService {
  final LocationApiClient locationApiClient;

  LocationService() : locationApiClient = LocationApiData();

  Future<List<Location>> getLocations() async {
    List<Location> list = await locationApiClient.getLocations();
    return list;
  }

  Future<Location> getLocation(int id) {
    return locationApiClient.getLocation(id);
  }
}
